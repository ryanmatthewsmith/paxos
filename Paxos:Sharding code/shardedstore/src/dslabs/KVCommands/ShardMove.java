package dslabs.KVCommands;

import dslabs.framework.Command;
import dslabs.atmostonce.AMOApplication;
import dslabs.framework.Address;
import java.util.Set;
import java.util.Map;
import lombok.Data;

@Data
public final class ShardMove implements Command {
    private final Map<Integer, AMOApplication> shardToApp;
    private final Address[]                    sendingAddress;
    private final int                          configNum;
}
