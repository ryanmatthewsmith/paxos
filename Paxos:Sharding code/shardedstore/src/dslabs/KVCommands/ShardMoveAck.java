package dslabs.KVCommands;

import dslabs.framework.Command;
import lombok.Data;
import java.util.Set;

@Data
public final class ShardMoveAck implements Command {
    private final Set<Integer> shards;
    private final int          configNum;
}
