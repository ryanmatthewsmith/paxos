package dslabs.KVCommands;

import dslabs.framework.Command;
import dslabs.shardmaster.ShardMaster.ShardConfig;
import lombok.Data;

//TODO:make AMOCommand non final and extend it OR make AMOCommand an interface and implement it with AMOCommand and all the new commands?
@Data
public final class NewConfig implements Command {
    private final ShardConfig shardConfig;
}