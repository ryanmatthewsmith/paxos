package dslabs.shardkv;

import dslabs.framework.Message;
import dslabs.atmostonce.AMOCommand;
import dslabs.atmostonce.AMOResult;
import dslabs.KVCommands.*;
import lombok.Data;

@Data
final class ShardStoreRequest implements Message {
	private final AMOCommand amoCommand;
	private final int        configNum;
}

@Data
final class ShardStoreReply implements Message {
	private final AMOResult amoResult;
	private final boolean   noGroup;
	private final int       configNum;
}

@Data
final class ShardMoveM implements Message{
	private final ShardMove shardMove;
}

@Data
final class ShardMoveAckM implements Message{
	private final ShardMoveAck shardMoveAck;
}


@Data
final class NewConfigM implements Message{
	private final NewConfig newConfig;
}
