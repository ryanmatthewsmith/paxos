package dslabs.shardkv;

import dslabs.KVCommands.ShardMove;
import dslabs.framework.Timeout;
import dslabs.framework.Address;
import java.util.Set;
import lombok.Data;

@Data
final class ClientTimeout implements Timeout {
	private static final int CLIENT_RETRY_MILLIS = 100;

	private final ShardStoreRequest request;

	@Override
	public int timeoutLengthMillis() {
		return CLIENT_RETRY_MILLIS;
	}
}

@Data
final class PingTimeout implements Timeout {
	private static final int PING_MILLIS = 50;

	@Override
	public int timeoutLengthMillis() {
		return PING_MILLIS;
	}
}

@Data
final class ShardMoveTimeout implements Timeout {
	private static final int SHARD_MOVE_MILLIS = 50;
	private final ShardMove shardMove;
	private final Set<Address> destination;

	@Override
	public int timeoutLengthMillis() {
		return SHARD_MOVE_MILLIS;
	}
}
