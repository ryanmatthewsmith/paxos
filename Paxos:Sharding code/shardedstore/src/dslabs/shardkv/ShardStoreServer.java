package dslabs.shardkv;

import dslabs.atmostonce.*;
import dslabs.kvstore.KVStore;
import dslabs.paxos.PaxosReply;
import dslabs.paxos.PaxosRequest;
import dslabs.paxos.PaxosServer;
import dslabs.framework.*;
import dslabs.shardmaster.ShardMaster;
import dslabs.shardmaster.ShardMaster.ShardConfig;
import dslabs.KVCommands.*;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.java.Log;
import org.apache.commons.lang3.tuple.Pair;

import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Log
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ShardStoreServer extends ShardStoreNode {
	public enum ConfigStatus{
		ACTIVE,
		WILL_RECONFIGURE,
		RECONFIGURING
	}

	private final Address[] group;
	private final int groupId;
	private final Map<Integer, AMOApplication> apps;
	private PaxosServer paxosNode;
	private static final String PAXOS_ADDRESS_ID = "paxos";
	private Address paxosAddress;

	private ShardConfig currentConfiguration;
	private ShardStoreRequest request; //TODO: maybe only allow one request at a time will be necessary, then client timeout to respark requests?
	private int sequenceNum;

	//new config vars
	private int shardsToMove;
	private int shardsToReceive;
	private Map<Integer, AMOApplication> appsToMove;
	private int ourGroupNum;
	private ConfigStatus configStatus;

	/* -------------------------------------------------------------------------
		Construction and initialization
	   -----------------------------------------------------------------------*/
	ShardStoreServer(Address address, Address[] shardMasters, int numShards,
	                 Address[] group, int groupId) {
		super(address, shardMasters, numShards);
		this.group = group;
		this.groupId = groupId;
		this.apps = new ConcurrentHashMap<>();
		this.paxosNode = null;
		this.currentConfiguration = null;
		this.sequenceNum = Integer.MIN_VALUE;

		this.shardsToMove = 0;
		this.shardsToReceive = 0;
		this.appsToMove = new HashMap<>();
		this.ourGroupNum = -1;
		this.configStatus = ConfigStatus.ACTIVE;
	}

	@Override
	public void init() {
		paxosAddress = Address.subAddress(address(), PAXOS_ADDRESS_ID);

		Address[] paxosAddresses = new Address[group.length];
		for(int i = 0; i < paxosAddresses.length; i++){
			paxosAddresses[i] = Address.subAddress(group[i], PAXOS_ADDRESS_ID);
		}

		PaxosServer paxosServer = new PaxosServer(paxosAddress, paxosAddresses, address());
		addSubNode(paxosServer);
		paxosServer.init();
		this.paxosNode = paxosServer;

		//start pinging servers, find out config
		broadcastToShardMasters(new PaxosRequest(new AMOCommand(new ShardMaster.Query(-1), sequenceNum++, this.address())));
		set(new PingTimeout());
	}


	/* -------------------------------------------------------------------------
		Message Handlers
	   -----------------------------------------------------------------------*/
	private synchronized void handleShardStoreRequest(ShardStoreRequest m, Address sender) {
		//no config
		if(currentConfiguration == null){
			return;
		}
		//handle out of date request
		if(currentConfiguration.configNum() > m.configNum()){
			//LOG.severe("NoGroup");
			send(new ShardStoreReply(null, true, currentConfiguration.configNum()), sender);
			return;
		}
        //reconfiguring
		if(configStatus != ConfigStatus.ACTIVE){
			return;
		}
		process(m.amoCommand(), false);
	}

	private synchronized void handleShardMoveM(ShardMoveM m, Address sender){
		if(currentConfiguration == null){
			return;
		}

		//out of date shard move
		if (m.shardMove().configNum() < currentConfiguration.configNum()){
			return;
		}
		//I appear to be behind config
		if (m.shardMove().configNum() > currentConfiguration.configNum()){
			//TODO: handle out of date config or leave it to ping?
			return;
		}
		boolean needsMove = false;
		for(int shard : m.shardMove().shardToApp().keySet()){
			if (!apps.containsKey(shard)) needsMove = true;
		}

		if(!needsMove){
			broadcast(new ShardMoveAckM(new ShardMoveAck(m.shardMove().shardToApp().keySet(), currentConfiguration.configNum())), m.shardMove().sendingAddress());
			return;
		}

		if(shardsToReceive == 0){// || configStatus != ConfigStatus.RECONFIGURING){
			//no shards receive
			return;
		}

		if(configStatus != ConfigStatus.RECONFIGURING){
			return;
		}
		process(m.shardMove(), false);
	}

	private synchronized void handleShardMoveAckM(ShardMoveAckM m, Address sender){
		//out of date shard move ack
		if (m.shardMoveAck().configNum() < currentConfiguration.configNum()){
			return;
		}
		//I appear to be behind config
		if (m.shardMoveAck().configNum() > currentConfiguration.configNum()){
			//TODO: handle out of date config?
			return;
		}
		if((shardsToMove == 0) || configStatus != ConfigStatus.RECONFIGURING){
			//not awaiting a move ack
			return;
		}
		process(m.shardMoveAck(), false);
	}

	private synchronized void handleNewConfigM(NewConfigM m, Address sender){
		//Im already at this config or ahead
		if (m.newConfig().shardConfig().configNum() <= currentConfiguration.configNum()){
			return;
		}
		if(configStatus != ConfigStatus.WILL_RECONFIGURE){
			return;
		}
		process(m.newConfig(), false);
	}

	private synchronized void handlePaxosDecision(PaxosDecision m, Address sender){
		process(m.amoCommand().command(), true);
	}

	private synchronized void handlePaxosReply(PaxosReply m, Address sender){
		ShardConfig nextConfig = null;
		if (m.amoResult().result() instanceof ShardConfig) {
			nextConfig = (ShardConfig) m.amoResult().result();
		} else{
			return;
		}

		//if (configStatus == ConfigStatus.ACTIVE && nextConfig != null
		if (nextConfig != null
				&& (currentConfiguration == null || nextConfig.configNum() > currentConfiguration.configNum())){
			//currentConfiguration = nextConfig;
			configStatus = ConfigStatus.WILL_RECONFIGURE;
			process(new NewConfig(nextConfig), false);
		}
	}

	/* -------------------------------------------------------------------------
		Timeout Handlers
	   -----------------------------------------------------------------------*/
	private synchronized void onPingTimeout(PingTimeout t){
		//if(configStatus == ConfigStatus.RECONFIGURING) {
			//LOG.severe(String.format("server %s config %s currentshards %s configState %s shardToMove %s shardsToReceive %s",
			//		this.address(), currentConfiguration, apps.keySet(), configStatus, shardsToMove, shardsToReceive));//this.address(), appsToMove, shardsToMove, shardsToReceive, configStatus));
		//}
		if(paxosNode.currentLeader()){ //&& configStatus == ConfigStatus.ACTIVE) {
			broadcastToShardMasters(new PaxosRequest(new AMOCommand(new ShardMaster.Query(-1), sequenceNum++, this.address())));
		}
		set(t);
	}

	private synchronized void onShardMoveTimeout(ShardMoveTimeout t){
		//LOG.severe(t.toString());
		if(shardsToMove > 0){
			broadcast(new ShardMoveM(t.shardMove()), t.destination());
			set(t);
		}
	}

	/* -------------------------------------------------------------------------
		Utils
	   -----------------------------------------------------------------------*/
	private void process(Command command, boolean replicated){
		if (command instanceof ShardMove){
			processShardMove((ShardMove)command, replicated);
		}
		else if (command instanceof ShardMoveAck){
			processShardMoveAck((ShardMoveAck)command, replicated);
		}
		else if (command instanceof NewConfig){
			processNewConfig((NewConfig)command, replicated);
		}
		else if (command instanceof AMOCommand){
			processAMOCommand((AMOCommand)command, replicated);
		}
	}

	private void processShardMove(ShardMove command, boolean replicated){
		if(!replicated){
			startPaxosForCommand(command);
		}
		else{
//			if(configStatus != ConfigStatus.RECONFIGURING){
//				return;
//			}
			if(command.configNum() == currentConfiguration.configNum() && shardsToReceive > 0){
				Set<Integer> shardsReceived = new HashSet<>();
				for(Map.Entry<Integer, AMOApplication> item : command.shardToApp().entrySet()){
					if(apps.containsKey(item.getKey())){
						LOG.warning(String.format("ShardMove %s processed already App %s Config %s", command, apps, currentConfiguration));
						//return;
					}else {
						apps.put(item.getKey(), item.getValue());
						shardsToReceive--;
						shardsReceived.add(item.getKey());
					}
				}
				if(!shardsReceived.isEmpty()){
					//TODO: this is bad, we should send groupnum
					for(Map.Entry<Integer, Pair<Set<Address>, Set<Integer>>> item : currentConfiguration.groupInfo().entrySet()) {

					}
					//TODO: make leader only send?
					if(paxosNode.currentLeader()) {
						broadcast(new ShardMoveAckM(new ShardMoveAck(shardsReceived, command.configNum())), command.sendingAddress());
					}
				}
			}
			if (shardsToMove == 0 && shardsToReceive == 0){
				configStatus = ConfigStatus.ACTIVE;
			}
		}

	}
	private void processShardMoveAck(ShardMoveAck command, boolean replicated){
		if(!replicated){
			startPaxosForCommand(command);
		}
		else{
//			if(configStatus != ConfigStatus.RECONFIGURING){
//				return;
//			}
			//TODO: config check, configStatus check
			if (shardsToMove <= 0){
				return;
			}
			Set<Integer> shardsReceived = command.shards();
			for (int shard : shardsReceived){
				if(appsToMove.containsKey(shard)){
					appsToMove.remove(shard);
					shardsToMove--;
				}
			}
			if (shardsToMove == 0 && shardsToReceive == 0){
				//TODO: assert appsToMove empty
				configStatus = ConfigStatus.ACTIVE;
			}
		}
	}
	private void processNewConfig(NewConfig command, boolean replicated){
		//TODO: any error handling?
		if(!replicated){
			startPaxosForCommand(command);
		}
		else{
			//if(configStatus != ConfigStatus.WILL_RECONFIGURE){
			//	return;
			//}
			shardsToReceive = 0;
			shardsToMove = 0;

			if(currentConfiguration == null && command.shardConfig().configNum() == 0){
				setupInitialConfiguration(command.shardConfig());
			}
			else{// if (configStatus == ConfigStatus.ACTIVE || configStatus == ConfigStatus.WILL_RECONFIGURE){
				setupNewConfiguration(command.shardConfig());
			}
		}

	}
	private void processAMOCommand(AMOCommand AMOcommand, boolean replicated){
		//TODO: any error handling?
		if(!replicated){
			startPaxosForCommand(AMOcommand);
		}
		//replicated
		else{
			String key = ((KVStore.SingleKeyCommand)AMOcommand.command()).key();
			int shard = keyToShard(key);
			AMOApplication app = apps.get(shard);
			if (app == null){
				LOG.fine(String.format("No app on %s command %s", this.address(), AMOcommand.command()));
				return;
			}
			AMOResult result = app.execute(AMOcommand);
			send(new ShardStoreReply(result, false, currentConfiguration.configNum()), result.clientAddress());
		}
	}
	private void startPaxosForCommand(Command command){
		if(paxosNode.currentLeader()){
			PaxosRequest request;
			if(command instanceof AMOCommand) {
				AMOCommand amoCommand = (AMOCommand) command;
				request = new PaxosRequest(new AMOCommand(amoCommand, amoCommand.sequenceNumber(), amoCommand.clientAddress()));
				//TODO: I made this paxos handler public to call direct on subnode, there probably is a better way that is escaping me
			}else{
				request = new PaxosRequest(new AMOCommand(command, sequenceNum, null));
			}
			paxosNode.handlePaxosRequest(request, this.address());
		}
	}
	private synchronized void setupInitialConfiguration(ShardConfig config){
		currentConfiguration = config;
		configStatus = ConfigStatus.RECONFIGURING;
		for(Map.Entry<Integer, Pair<Set<Address>, Set<Integer>>> item : currentConfiguration.groupInfo().entrySet()) {
			ourGroupNum = item.getKey();
			if (item.getValue().getLeft().contains(this.address())){
				for (int shard : item.getValue().getRight()){
					apps.put(shard, new AMOApplication(new KVStore()));
				}
				configStatus = ConfigStatus.ACTIVE;
				return;
			}
		}
	}
	private synchronized void setupNewConfiguration(ShardConfig config){
		currentConfiguration = config;
		configStatus = ConfigStatus.RECONFIGURING;
		appsToMove = new HashMap<>();
		for(Map.Entry<Integer, Pair<Set<Address>, Set<Integer>>> item : currentConfiguration.groupInfo().entrySet()) {
			if (item.getValue().getLeft().contains(this.address())){
				ourGroupNum = item.getKey();
				for (int shard : item.getValue().getRight()){
					if(!apps.containsKey(shard)) {
						shardsToReceive++;
					}
				}
				for (int currentShard : apps.keySet()){
					if(!item.getValue().getRight().contains(currentShard)){
						appsToMove.put(currentShard, apps.remove(currentShard));
						shardsToMove++;
					}
				}
				performShardMoves();
				if(shardsToMove == 0 && shardsToReceive == 0){
					configStatus = ConfigStatus.ACTIVE;
				}
				return;
			}
		}
		//no longer in config, need to move out shards
		if(apps.size() > 0){
			for(int currentShard : apps.keySet()){
				appsToMove.put(currentShard, apps.remove(currentShard));
				shardsToMove++;
			}
		}
		performShardMoves();
	}

	private void performShardMoves(){
		for (Map.Entry<Integer, Pair<Set<Address>, Set<Integer>>> item : currentConfiguration.groupInfo().entrySet()) {
			Map<Integer, AMOApplication> shardMove = new HashMap<>();
			for (Integer shard : appsToMove.keySet()) {
				if (item.getValue().getRight().contains(shard)) {
					shardMove.put(shard, appsToMove.get(shard));
				}
			}
			//TODO: will likely need timeout and uid for shardmoves, probably should only send by leader but what if we dont have a leader atm?
			if (!shardMove.isEmpty() && paxosNode.currentLeader()) {
				ShardMove currentShardMove = new ShardMove(shardMove, this.group, currentConfiguration.configNum());
				broadcast(new ShardMoveM(currentShardMove), item.getValue().getLeft());
				set(new ShardMoveTimeout(currentShardMove, item.getValue().getLeft()));
			}
		}
	}
}
