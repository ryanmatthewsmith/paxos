package dslabs.shardkv;

import dslabs.framework.Address;
import dslabs.framework.Client;
import dslabs.framework.Command;
import dslabs.framework.Result;
import dslabs.atmostonce.AMOCommand;
import dslabs.kvstore.KVStore;
import dslabs.paxos.PaxosReply;
import dslabs.paxos.PaxosRequest;
import dslabs.shardmaster.ShardMaster;
import dslabs.shardmaster.ShardMaster.ShardConfig;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.java.Log;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;
import java.util.Set;


@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Log
public class ShardStoreClient extends ShardStoreNode implements Client {
	private ShardStoreRequest request;
	private ShardStoreReply   reply;
	private int               sequenceNum;
	private int               configSequenceNum;
	private ShardConfig       currentConfiguration;
	private Command			  startCommand;
	/* -------------------------------------------------------------------------
		Construction and Initialization
	   -----------------------------------------------------------------------*/
	public ShardStoreClient(Address address, Address[] shardMasters,
	                        int numShards) {
		super(address, shardMasters, numShards);
		sequenceNum = 0;
		configSequenceNum = 0;
		currentConfiguration = null;
		startCommand = null;
	}

	@Override
	public synchronized void init() {
		PaxosRequest r = new PaxosRequest(new AMOCommand(new ShardMaster.Query(-1), configSequenceNum++, this.address()));
		broadcastToShardMasters(r);
	}

	/* -------------------------------------------------------------------------
		Public methods
	   -----------------------------------------------------------------------*/
	@Override
	public synchronized void sendCommand(Command command) {
		ShardStoreRequest r;

		if (currentConfiguration != null){
			LOG.fine(command.toString());
			r = new ShardStoreRequest(new AMOCommand(command, sequenceNum++, this.address()), currentConfiguration.configNum());

			request = r;
			reply = null;
			broadcastRequest();
		}else{
			startCommand = command;
			broadcastToShardMasters(new PaxosRequest(new AMOCommand(new ShardMaster.Query(-1), configSequenceNum, this.address())));
			set(new ClientTimeout(null));
			return;
		}

		set(new ClientTimeout(request));
	}

	@Override
	public synchronized boolean hasResult()  {
		return reply != null;
	}

	@Override
	public synchronized Result getResult() throws InterruptedException {
		while (!this.hasResult()){
			wait();
		}

		Result returnResult = reply.amoResult().result();
		reply = null;
		return returnResult;
	}

	/* -------------------------------------------------------------------------
		Message Handlers
	   -----------------------------------------------------------------------*/
	private synchronized void handleShardStoreReply(ShardStoreReply m,
	                                                Address sender) {
		LOG.fine(String.format("ShardStoreReply %s, From %s", m, sender));
		//noGroup handle
		if (m.noGroup()){
			if (m.configNum() > currentConfiguration.configNum()){
				//TODO: this seems wrong, ask for help
				broadcastToShardMasters(new PaxosRequest(new AMOCommand(new ShardMaster.Query(m.configNum()), configSequenceNum++, this.address())));
			}
		}
		else if (request != null
				&& m.amoResult().sequenceNumber() == request.amoCommand().sequenceNumber()
				&& m.amoResult().clientAddress().equals(this.address())){

			LOG.fine("success " + m.amoResult().result());
			reply = m;
			request = null;
			notify();
		}
	}

	private synchronized void handlePaxosReply(PaxosReply m, Address sender){
		//notify();
		if (m.amoResult().result() instanceof ShardConfig){
			ShardConfig nextConfig = (ShardConfig)m.amoResult().result();
			LOG.fine("Client received PaxosReply result: " + nextConfig.toString());
			if (currentConfiguration != null && nextConfig.configNum() <= currentConfiguration.configNum())
				return;
			currentConfiguration = nextConfig;
			if (request != null) {
				request = new ShardStoreRequest(request.amoCommand(), this.currentConfiguration.configNum());
			}
		}
	}

	/* -------------------------------------------------------------------------
		Timeout Handlers
	   -----------------------------------------------------------------------*/
	private synchronized void onClientTimeout(ClientTimeout t) {
		if(t.request() != null) {
			//LOG.severe(String.format("Timeout: %s", t.request().amoCommand()));
		}
		if(currentConfiguration == null){
			broadcastToShardMasters(new PaxosRequest(new AMOCommand(new ShardMaster.Query(-1), configSequenceNum++, this.address())));
			set(t);
			return;
		}

		if(reply == null && request != null && t.request().amoCommand().sequenceNumber() == request.amoCommand().sequenceNumber()){// && t.request().equals(this.request)){
			broadcastRequest();
			set(t);
		}
		else if(startCommand != null && currentConfiguration.configNum() == 0){
			sendCommand(startCommand);
			startCommand = null;
		}
	}

	private synchronized void broadcastRequest(){
		String key = ((KVStore.SingleKeyCommand)request.amoCommand().command()).key();
		int shard = keyToShard(key);
		for(Map.Entry<Integer, Pair<Set<Address>, Set<Integer>>> item : currentConfiguration.groupInfo().entrySet()) {
			if (item.getValue().getRight().contains(shard)) {
				//LOG.severe(key + " " + shard + " " + item.getValue().getLeft().toString() + currentConfiguration);
				broadcast(this.request, item.getValue().getLeft());
				set(new ClientTimeout(this.request));
				return;
			}
		}
		LOG.severe(String.format("missing shard %s client %s currentConfig %s", shard, this.address(), this.currentConfiguration));
		set(new ClientTimeout(this.request));
	}
	
}
