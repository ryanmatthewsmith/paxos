package dslabs.shardmaster;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;

import dslabs.paxos.PaxosReply;
import dslabs.paxos.PaxosRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.java.Log;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import dslabs.framework.*;

@Log
@ToString
@EqualsAndHashCode
public final class ShardMaster implements Application {
	public static final int INITIAL_CONFIG_NUM = 0;
	private final int numShards;

	// ShardConfig may or may not be what we want to store.
	private List<ShardConfig> shardConfigs = new ArrayList<>();

	public ShardMaster(int numShards) {
		this.numShards = numShards;
	}

	public interface ShardMasterCommand extends Command {
	}

	@Data
	public static final class Join implements ShardMasterCommand {
		private final int groupId;
		private final Set<Address> servers;
	}

	@Data
	public static final class Leave implements ShardMasterCommand {
		private final int groupId;
	}

	@Data
	public static final class Move implements ShardMasterCommand {
		private final int groupId;
		private final int shardNum;
	}

	@Data
	public static final class Query implements ShardMasterCommand {
		private final int configNum;

		@Override
		public boolean readOnly() {
			return true;
		}
	}

	public interface ShardMasterResult extends Result {
	}

	@Data
	public static final class Ok implements ShardMasterResult {
	}

	@Data
	public static final class Error implements ShardMasterResult {
	}

	@Data
	public static final class ShardConfig implements ShardMasterResult {
		private final int configNum;

		// groupId -> <group members, shard numbers>
		private final Map<Integer, Pair<Set<Address>, Set<Integer>>> groupInfo;
		
	}

	@Override
	public Result execute(Command command) {
		if (command instanceof Join) {
			Join join = (Join) command;
			return this.executeJoin(join);
		}

		if (command instanceof Leave) {
			Leave leave = (Leave) command;
			return this.executeLeave(leave);
		}

		if (command instanceof Move) {
			Move move = (Move) command;
			return this.executeMove(move);
		}

		if (command instanceof Query) {
			Query query = (Query) command;
			return this.executeQuery(query);
		}

		throw new IllegalArgumentException();
	}

	private ShardMasterResult executeJoin(Join join) {
		ShardConfig previousConfiguration;
		int nextConfigurationNumber = shardConfigs.isEmpty() ?
				                      INITIAL_CONFIG_NUM : latestConfigurationNumber() + 1;

		//case: initial configuration
		if(nextConfigurationNumber == INITIAL_CONFIG_NUM) {
			setupInitialConfiguration(join.groupId, join.servers);
			return new Ok();
		}

		previousConfiguration = latestConfiguration();

		//case: group already exists
		if (previousConfiguration.groupInfo.containsKey(join.groupId())){
			return new Error();
		}

		//case: normal group join
		newConfigurationJoin(previousConfiguration, join);
		redistributeShards(new HashSet<Integer>());

		return new Ok();
	}

	private ShardMasterResult executeLeave(Leave leave) {
		ShardConfig previousConfiguration;
		previousConfiguration = latestConfiguration();

		if (!previousConfiguration.groupInfo.containsKey(leave.groupId())){
			return new Error();
		}

		Set<Integer> hangingShards = newConfigurationLeave(previousConfiguration, leave);
		redistributeShards(hangingShards);

		return new Ok();
	}

	private ShardMasterResult executeMove(Move move) {
		//invalid shard number
		if(move.shardNum() < 1 || move.shardNum() > numShards){
			return new Error();
		}

		ShardConfig previousConfiguration;
		previousConfiguration = latestConfiguration();

		//group missing
		if(!previousConfiguration.groupInfo.containsKey(move.groupId)){
			return new Error();
		}

		//already contains shard on group
		if(previousConfiguration.groupInfo.get(move.groupId).getRight().contains(move.shardNum)){
			return new Error();
		}

		newConfigurationMove(previousConfiguration, move);

		return new Ok();
	}

	private ShardMasterResult executeQuery(Query query) {
		if (this.shardConfigs.size() == 0) {
			return new Error();
		}

		int shardIndex = query.configNum;

		if (shardIndex == -1 || shardIndex > this.shardConfigs.size()) {
			shardIndex = this.shardConfigs.size() - 1;
		}

		return this.shardConfigs.get(shardIndex);
	}

	private ShardConfig latestConfiguration(){
		if (!shardConfigs.isEmpty()) {
			return shardConfigs.get(shardConfigs.size() - 1);
		}
		else
			return null;
	}

	private int latestConfigurationNumber(){
		return shardConfigs.size() - 1;
	}

	private void setupInitialConfiguration(int groupId, Set<Address> servers){
		Set<Address> a = servers;
		Set<Integer> b = new HashSet();

		//add shards
		for (int i = 1; i <= numShards; i++){
			b.add(i);
		}

		Pair<Set<Address>, Set<Integer>> servers_shards = new ImmutablePair<>(a, b);
		ShardConfig initialConfig = new ShardConfig(INITIAL_CONFIG_NUM, new HashMap());
		initialConfig.groupInfo.put(groupId, servers_shards);
		shardConfigs.add(initialConfig);
	}

	private void newConfigurationJoin(ShardConfig previousConfiguration, Join join){
		Pair<Set<Address>, Set<Integer>> servers_shards = new ImmutablePair<>(join.servers, new HashSet());
		ShardConfig newConfiguration = new ShardConfig(latestConfigurationNumber() +1, new HashMap());
		newConfiguration.groupInfo.put(join.groupId, servers_shards);

		for(Map.Entry<Integer, Pair<Set<Address>, Set<Integer>>> item : previousConfiguration.groupInfo.entrySet()){
			newConfiguration.groupInfo.put(item.getKey(), item.getValue());
		}

		shardConfigs.add(newConfiguration);
	}

	private Set<Integer> newConfigurationLeave(ShardConfig previousConfiguration, Leave leave){
		Set<Integer> hangingShards = new HashSet<>();
		ShardConfig newConfiguration = new ShardConfig(latestConfigurationNumber() +1, new HashMap());

		for(Map.Entry<Integer, Pair<Set<Address>, Set<Integer>>> item : previousConfiguration.groupInfo.entrySet()){
			if (item.getKey() != leave.groupId()) {
				newConfiguration.groupInfo.put(item.getKey(), item.getValue());
			}else{
				hangingShards = item.getValue().getRight();
			}
		}
		shardConfigs.add(newConfiguration);

		return hangingShards;
	}

	private void newConfigurationMove(ShardConfig previousConfiguration, Move move){
		ShardConfig newConfiguration = new ShardConfig(latestConfigurationNumber() +1, new HashMap());

		for(Map.Entry<Integer, Pair<Set<Address>, Set<Integer>>> item : previousConfiguration.groupInfo.entrySet()){
			if(item.getValue().getRight().contains(move.shardNum)){
				Set<Integer> toRemoveShard = new HashSet<>();
				for(Integer shard : item.getValue().getRight()){
					if(shard != move.shardNum){
						toRemoveShard.add(shard);
					}
				}
				Pair<Set<Address>, Set<Integer>> servers_shards = new ImmutablePair<>(item.getValue().getLeft(), toRemoveShard);
				newConfiguration.groupInfo.put(item.getKey(), servers_shards);
			}
			else if (item.getKey() != move.groupId()) {
				newConfiguration.groupInfo.put(item.getKey(), item.getValue());
			}
			else{
				Set<Integer> toAddShard  = new HashSet<>();
				toAddShard.add(move.shardNum);
				for(Integer shard : item.getValue().getRight()){
					toAddShard.add(shard);
				}
				Pair<Set<Address>, Set<Integer>> servers_shards = new ImmutablePair<>(item.getValue().getLeft(), toAddShard);
				newConfiguration.groupInfo.put(item.getKey(), servers_shards);
			}
		}
		shardConfigs.add(newConfiguration);

	}

	private void redistributeShards(Set hangingShards){
		ShardConfig preDistributedConfigs = latestConfiguration();
		if(preDistributedConfigs == null || shardConfigs.size() < 2){
			return;
		}
		int numberOfGroups = preDistributedConfigs.groupInfo.keySet().size();
		int distributionTarget = numShards / numberOfGroups;
		int remainderToKeep = -1;
		if(hangingShards.isEmpty()){
			remainderToKeep = numShards % numberOfGroups;
		}

		Map<Integer, Set<Integer>> distributionMap = new HashMap<>();

		//loadMap with old values where possible
		for(Map.Entry<Integer, Pair<Set<Address>, Set<Integer>>> item : preDistributedConfigs.groupInfo.entrySet()){
			Set<Integer> existingGroupShards = new HashSet<>();
			distributionMap.put(item.getKey(), existingGroupShards);

			int count = 0;
			for(Integer shard : item.getValue().getRight()) {
				if (count < distributionTarget) {
					existingGroupShards.add(shard);
					count++;
				} else if (count == distributionTarget && remainderToKeep > 0){
					existingGroupShards.add(shard);
					count++;
					remainderToKeep--;
				}
				else{
					hangingShards.add(shard);
				}
			}
		}

		if(hangingShards.size() < 1){
			return;//nothing to redistribute
		}

		List<Integer> hangingShardsList = new ArrayList<Integer>(hangingShards);

		//bring all groups up to distribution target
		for(Set<Integer> shards : distributionMap.values()){
			while (shards.size() < distributionTarget  && !hangingShardsList.isEmpty()){
				shards.add(hangingShardsList.remove(0));
			}
		}

		if(hangingShardsList.size() >= distributionTarget){
			//TODO: Add Assertion / Error
		}

		//evenly distribute remaining shards
		while(!hangingShardsList.isEmpty()){
			for(Set<Integer> shards : distributionMap.values()){
				if(hangingShardsList.isEmpty())
					break;

				shards.add(hangingShardsList.remove(0));
			}
		}

		//create redistributed version of Configuration
		shardConfigs.remove(shardConfigs.size() - 1);

		ShardConfig redistributedConfiguration = new ShardConfig(latestConfigurationNumber() +1, new HashMap());

		for(Map.Entry<Integer, Set<Integer>> item : distributionMap.entrySet()){
			Pair<Set<Address>, Set<Integer>> servers_shards = new ImmutablePair<>(preDistributedConfigs.groupInfo.get(item.getKey()).getLeft(), item.getValue());
			redistributedConfiguration.groupInfo.put(item.getKey(), servers_shards);
		}
		shardConfigs.add(redistributedConfiguration);
		LOG.fine("Checking redistribution:");
		for(Map.Entry<Integer, Pair<Set<Address>, Set<Integer>>> item :redistributedConfiguration.groupInfo.entrySet()){
			LOG.fine(String.format("group: %s shards: %s shardssize: %s", item.getKey(), item.getValue(), item.getValue().getRight().size()));
		}
	}

	//TODO: how do we interact with the ShardMaster properly?
	private void handlePaxosRequest(PaxosRequest m, Address sender){
		LOG.severe("paxos Request message received ShardMaster");
		if(m.amoCommand().command() instanceof Query){
			ShardMasterResult res = executeQuery((Query)m.amoCommand().command());
			if (!(res instanceof Error)){
				//TODO: how do we send this
			}
		}
	}
}
