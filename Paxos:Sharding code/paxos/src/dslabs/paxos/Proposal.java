package dslabs.paxos;

import dslabs.atmostonce.AMOCommand;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;

@ToString(callSuper = true)
@EqualsAndHashCode()
public class Proposal implements Serializable {
	public int        proposalNumber;
	public AMOCommand proposalValue;

	public Proposal(int proposalNumber, AMOCommand proposalValue){
		this.proposalNumber = proposalNumber;
		this.proposalValue = proposalValue;
	}
}
