package dslabs.paxos;

import dslabs.atmostonce.AMOCommand;
import dslabs.atmostonce.AMOApplication;
import dslabs.atmostonce.AMOResult;
import dslabs.framework.Address;
import dslabs.framework.Application;
import dslabs.framework.Node;
import dslabs.shardkv.PaxosDecision;

import java.util.*;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.java.Log;

@Log
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class PaxosServer extends Node {
	/* --------------------------
	    Instance Variables
	   --------------------------*/
	AMOApplication app;
	private final Address[] servers;
	private final int thisServerIndex;

	public enum Role{
		LEADER,
		LEADER_CANDIDATE,
		PEON
	}

	private int firstUnchosenSlot;
	private int firstUnexecutedSlot;
	private int lastExecutedSlot;
	private int firstEmptySlot;
	private int firstLogSlot = 0;
	private Address decisionClient;

	HashMap<Integer, PaxosInstance> log;
	HashMap<Address, Integer> clientSlots;
	Map<Address, Integer> peerExecutionInfo;
	Map<Integer, Map<Address, Proposal>> acceptedProposals;
	Set<Integer> leaderConfirms;
	Role serverRole;

	Map<Address, Integer> heartbeats;

	/* -------------------------------------------------------------------------
		Construction and Initialization
	   -----------------------------------------------------------------------*/
	public PaxosServer(Address address, Address[] servers, Application app) {
		super(address);
		this.servers = servers;
		this.app = new AMOApplication(app);

		sortAddresses(servers);

		heartbeats = new HashMap<>();
		this.log = new HashMap<>();
		this.clientSlots = new HashMap<>();
		this.peerExecutionInfo = new HashMap<>();
		this.acceptedProposals = new HashMap<>();
		this.serverRole = Role.PEON;

		this.thisServerIndex = getSeedValue();
		this.firstUnchosenSlot = 0;
		this.lastExecutedSlot = -1;
		this.firstUnexecutedSlot = 0;
		this.firstEmptySlot = 0;

		for (Address server : servers){
			if(!server.equals(this.address())){
				heartbeats.put(server, 0);
			}

			this.peerExecutionInfo.put(server, -1);
		}
		decisionClient = null;
	}

	public PaxosServer(Address address, Address[] servers, Address decision) {
		super(address);
		this.servers = servers;
		this.app = null;

		sortAddresses(servers);

		heartbeats = new HashMap<>();
		this.log = new HashMap<>();
		this.clientSlots = new HashMap<>();
		this.peerExecutionInfo = new HashMap<>();
		this.acceptedProposals = new HashMap<>();
		this.serverRole = Role.PEON;

		this.thisServerIndex = getSeedValue();
		this.firstUnchosenSlot = 0;
		this.lastExecutedSlot = -1;
		this.firstUnexecutedSlot = 0;
		this.firstEmptySlot = 0;

		for (Address server : servers){
			if(!server.equals(this.address())){
				heartbeats.put(server, 0);
			}

			this.peerExecutionInfo.put(server, -1);
		}

		decisionClient = decision;
	}

	@Override
	public void init() {
		set(new HeartbeatTimeout());
	}

	/* -------------------------------------------------------------------------
		Message Handlers
	   -----------------------------------------------------------------------*/
	public synchronized void handlePaxosRequest(PaxosRequest m, Address sender) {
		//LOG.severe("request: " + m);

		LOG.fine(String.format("handlePaxosRequest(%s, %s) by %s", m, sender, this.address()));

		Integer clientSlot = this.clientSlots.get(sender);

		if (clientSlot != null) {
			PaxosInstance instance = this.log.get(clientSlot);
			if (instance == null) {
				return;
			}

			if (instance.executed && instance.proposal.proposalValue.sequenceNumber() == m.amoCommand().sequenceNumber()) {
				// Re-send the PaxosReply.
				if(app != null) {
					this.send(new PaxosReply(this.app.execute(instance.proposal.proposalValue)), instance.proposal.proposalValue.clientAddress());
				} else{
					handleMessage(new PaxosDecision(instance.proposal.proposalValue), decisionClient);
				}
				return;
			}
		}

		if (serverRole != Role.LEADER) {
			return; //not leader
		}

		if (this.allowPaxosRequest(sender)) {
			//new slot
			int allocatedSlotNum = this.firstEmptySlot;
			this.clientSlots.put(sender, allocatedSlotNum);
			this.firstEmptySlot++;

			LOG.fine(String.format("%s: %s allocated slot %d", this.address(), sender, allocatedSlotNum));

			Proposal prop = new Proposal(this.thisServerIndex, m.amoCommand());

			LOG.fine(String.format("Accept, num: %s slot: %s", prop.proposalNumber, allocatedSlotNum));
			broadcastAcceptRequest(new AcceptRequest(prop, allocatedSlotNum, this.firstUnchosenSlot));
		}
		else {
			LOG.fine(String.format("Client %s not allowed paxos request.", sender));
		}
	}

	private synchronized void handleAcceptRequest(AcceptRequest m, Address sender) {
		LOG.fine(String.format("handleAcceptRequest(%s, %s)", m, sender));

		if (m.firstUnchosenSlot() > firstUnchosenSlot){
			//need update
		}

		if (m.slotNum() < this.firstLogSlot) {
			// GC'd slot.
			return;
		}

		boolean accept = false;

		if (m.proposedValue() == null) {
			LOG.fine("someone proposed value is null AcceptRequest");
			//return;
		}

		if (log.get(m.slotNum()) == null) {
			addLogData(m.slotNum());
		}

		while (this.log.containsKey(this.firstEmptySlot)) {
			this.firstEmptySlot++;
		}

		PaxosInstance pi = log.get(m.slotNum());

		if (m.proposedValue().proposalNumber >= pi.proposal.proposalNumber) {
			accept = true;
			pi.proposal = m.proposedValue();
		}

		LOG.fine(String.format("AcceptResponse, highPropNum: %s slot: %s from: %s",
				m.proposedValue().proposalNumber, m.slotNum(), this.address()));
		send(new AcceptResponse(m.slotNum(), accept, pi.proposal, pi.chosen, firstUnchosenSlot), sender);
	}

	private synchronized void handleAcceptResponse(AcceptResponse m, Address sender) {
		LOG.fine(String.format("handleAcceptResponse(%s, %s)", m, sender));

		if (m.slotNum() < this.firstLogSlot) {
			// GC'd slot.
			return;
		}

		if(!acceptedProposals.containsKey(m.slotNum())){
			acceptedProposals.put(m.slotNum(), new HashMap<>());
		}

		if (serverRole != Role.LEADER) {
			LOG.fine(this.address() + "no longer leader");
			return;
		}

		PaxosInstance pi = log.get(m.slotNum());

		if (pi == null) {
			pi = new PaxosInstance(m.slotNum());
			log.put(m.slotNum(), pi);
			LOG.fine("PaxosInstance was Missing");
		}
		if (this.firstUnchosenSlot > m.slotNum()) {
			LOG.fine("dated request, value already chosen");
			//return;
		}

		//add chosen timeout? or handle in heartbeat
		if (!m.accepted() && !m.chosen()) {
			if (m.acceptedProposal().proposalNumber > thisServerIndex) {
				LOG.fine(String.format("Not accepted (slotNum=%d); re-sending accept.", m.slotNum()));
				pi.proposal = (m.acceptedProposal());
			}

			this.broadcast(new AcceptRequest(m.acceptedProposal(), m.slotNum(), this.firstUnchosenSlot), this.servers);
			return;
		}

		//this is for the addresses accepting proposal
		addAcceptedAtSlot(m.slotNum(), m.acceptedProposal(), sender);

		//already chosen value
		if(m.chosen() && !pi.chosen){
			pi.proposal = m.acceptedProposal();
			pi.chosen   = true;
			updateLowestUnchosenSlot(m.slotNum());
			executeLog();
			return;
		}

		if (acceptedProposals.get(m.slotNum()).values().size() >= ((servers.length / 2) + 1)) {
			LOG.fine("majority accepted prop");
			if (checkChosenValue(m.slotNum(), m.acceptedProposal())) {
				LOG.fine(String.format("Chosen, highPropNum: %s slot: %s from: %s", m.acceptedProposal().proposalNumber, m.slotNum(), this.address()));
				broadcastChosen(new Chosen(m.slotNum(), m.acceptedProposal()));
			}
		}
	}

	private synchronized void handleChosen(Chosen m, Address sender) {
		LOG.fine(String.format("handleChosen(%s, %s)", m, sender));

		if (m.slotNum() < this.firstLogSlot) {
			// GC'd slot.
			return;
		}

		addLogData(m.slotNum());

		while (this.log.containsKey(this.firstEmptySlot)) {
			this.firstEmptySlot++;
		}

		PaxosInstance pi = log.get(m.slotNum());

		if (pi.executed) {
			LOG.fine("value previously executed");
			return; //already performed
		}

		pi.chosen = true;

		assert !pi.executed;

		updateLowestUnchosenSlot(m.slotNum());
		pi.proposal = m.chosenProposal();

		this.executeLog();
	}

	private synchronized void execute(PaxosInstance instance) {
		if (instance.executed) {
			LOG.fine(String.format("Already executed %s.", instance));
			return;
		}

		LOG.fine(String.format("server %s executing %s", this.address(), instance));

		if (instance == null || instance.proposal.proposalValue == null) {
			LOG.fine("execute called on null PI");
			return;
		}

		assert this.lastExecutedSlot == instance.slotNum - 1;


		instance.executed = true;
		this.lastExecutedSlot = instance.slotNum;
		this.peerExecutionInfo.put(this.address(), this.lastExecutedSlot);

		//if (isLeader) {
		LOG.fine(String.format("chosen value %s slotnum %s returned by %s",
				instance.proposal.proposalValue, instance.slotNum, this.address()));

		if(app != null) {
			AMOResult res = app.execute(instance.proposal.proposalValue);
			Address clientAddr = instance.proposal.proposalValue.clientAddress();
			this.send(new PaxosReply(res), clientAddr);
		} else{
			//handleMessage(message, ShardStoreAddress);
			handleMessage(new PaxosDecision(instance.proposal.proposalValue), decisionClient);
		}
		//}
	}

	private synchronized void handleHeartbeat(Heartbeat m, Address sender) {
		if(this.address().equals(sender)){
			return;
		}
		//LOG.fine("heartbeat " + address() + " " + heartbeats.toString());
		heartbeats.put(sender, 0);

		// Reconcile log.

		boolean didChangeLog = false;
		int minChangedSlot = Integer.MAX_VALUE;

		for (Map.Entry<Integer, PaxosInstance> entrySet : m.log().entrySet()) {
			Integer slotNum = entrySet.getKey();
			PaxosInstance remoteInstance = entrySet.getValue();
			PaxosInstance localInstance = this.log.get(slotNum);

			if (slotNum >= this.firstLogSlot) {
				if (localInstance == null
						|| (remoteInstance.chosen && !localInstance.chosen)) {
					LOG.fine(String.format("%s LOG adopting chosen remote value for slot %d",
							this.address(), slotNum));
					remoteInstance.executed = false;
					this.log.put(slotNum, remoteInstance);

					didChangeLog = true;
					minChangedSlot = Integer.min(minChangedSlot, slotNum);
				} else if(remoteInstance.proposal.proposalValue != null &&
						      !localInstance.chosen &&
						      (localInstance.proposal.proposalValue == null ||
						      localInstance.proposal.proposalNumber <= remoteInstance.proposal.proposalNumber)){
					this.log.put(slotNum, remoteInstance);
				}
			}
		}

		if (didChangeLog) {
			while (this.log.containsKey(this.firstEmptySlot)) {
				this.firstEmptySlot++;
			}
			this.updateLowestUnchosenSlot(minChangedSlot);
			this.executeLog(false);
		}

		// Reconcile client map.

		for (Map.Entry<Address, Integer> entrySet : m.clientSlots().entrySet()) {
			Address address = entrySet.getKey();
			Integer remoteSlot = entrySet.getValue();
			Integer localSlot = this.clientSlots.get(address);

			if (remoteSlot >= this.firstLogSlot && (localSlot == null || remoteSlot > localSlot)) {
				LOG.fine(String.format("%s adopting newer clientSlot %s=%d",
						this.address(), address, remoteSlot));
				this.clientSlots.put(address, remoteSlot);
			}
		}

		this.peerExecutionInfo.put(sender, m.lastExecutedSlot());

		this.collectGarbage();

		if(serverRole == Role.LEADER) {
			for (Map.Entry<Integer, PaxosInstance> logentry : log.entrySet()) {
				PaxosInstance instance = logentry.getValue();
				if (!instance.chosen) {
					broadcastAcceptRequest(new AcceptRequest(instance.proposal, logentry.getKey(), firstUnchosenSlot));
				}
			}
		}
	}

	private void handleLeaderConfirmation(LeaderConfirmation m, Address sender) {
		Heartbeat heartbeat = new Heartbeat(this.thisServerIndex, this.log, this.clientSlots,
				this.lastExecutedSlot, serverRole == Role.LEADER, this.firstUnchosenSlot, this.firstEmptySlot);
		send(new LeaderConfirmationResponse(thisServerIndex, heartbeat), sender);
	}

	private void handleLeaderConfirmationResponse(LeaderConfirmationResponse m, Address sender) {
		if (m.serverIndex() > thisServerIndex){
			leaderConfirms.add(-1);
		} else{
			leaderConfirms.add(m.serverIndex());
		}

		this.handleHeartbeat(m.heartbeat(), sender);
	}

	/* -------------------------------------------------------------------------
		Timeout Handlers
	   -----------------------------------------------------------------------*/
	private synchronized void onHeartbeatTimeout(HeartbeatTimeout t) {

		incrementHeartbeatMisses();

		updateLeaderStatus();

		LOG.fine("bcast HEARTBEAT from " + this.address());

		Heartbeat heartbeat = new Heartbeat(this.thisServerIndex, this.log, this.clientSlots,
				this.lastExecutedSlot, serverRole == Role.LEADER, this.firstUnchosenSlot, this.firstEmptySlot);

		this.broadcast(heartbeat, this.heartbeats.keySet());
		this.set(t);
	}

	private synchronized void onLeaderConfirmationTimeout(LeaderConfirmationTimeout t){
		if(serverRole != Role.LEADER_CANDIDATE){
			return;
		}
		if(leaderConfirms.contains(-1)){
			serverRole = Role.PEON;
			return;
		}
		if (leaderConfirms.size() >= (servers.length/2)){
			LOG.fine(String.format("NEW LEADER: %s", this.address()));
			serverRole = Role.LEADER;
			acceptedProposals = new HashMap<>();
			return;
		}
		serverRole = Role.PEON;
//		for(Address server : servers){
//			if(!this.address().equals(server) && !leaderConfirms.contains(server)){
//				send(new LeaderConfirmation(), server);
//			}
//		}
//		set(t);
	}

	/* -------------------------------------------------------------------------
		Utils
	   -----------------------------------------------------------------------*/

	private int getSeedValue() {
		return Arrays.asList(servers).indexOf(this.address());
	}

	private void sortAddresses(Address[] servers) {
		Arrays.sort(servers, (a, b) -> a.compareTo(b));
	}

	private boolean allowPaxosRequest(Address clientAddress) {
		Integer clientSlot = this.clientSlots.get(clientAddress);

		if (clientSlot == null) {
			return true;
		} else {
			PaxosInstance instance = this.log.get(clientSlot);
			// They can do a new request if this one is executed.
			return instance.executed;
		}
	}

	private void addLogData(int slotNum) {
		if (log.get(slotNum) == null) {
			this.log.put(slotNum, new PaxosInstance(slotNum));
		}
	}

	private void addAcceptedAtSlot(int slotNum, Proposal proposal, Address server){
		if (slotNum < this.firstLogSlot) {
			// GC'd slot.
			return;
		}

		if(!acceptedProposals.containsKey(slotNum)){
			acceptedProposals.put(slotNum, new HashMap());
		}

		Map<Address, Proposal> p = acceptedProposals.get(slotNum);
		p.put(server, proposal);
	}

	private boolean checkChosenValue(int slotNum, Proposal acceptedProposal) {
		int target = ((servers.length / 2) + 1);
		int found = 0;

		for (Proposal p : this.acceptedProposals.get(slotNum).values()) {
			if (p.equals(acceptedProposal)) found++;
			if (found >= target) return true;
			// LOG.fine(String.format("found: %s size: %s", found, this.acceptedProposals.get(slotNum).values().size()));
		}

		return false;
	}

	private void updateLowestUnchosenSlot(int slotNum) {
		if (slotNum == this.firstUnchosenSlot) {
			this.firstUnchosenSlot++;
			PaxosInstance next = this.log.get(this.firstUnchosenSlot);
			while (next != null && next.chosen) {
				this.firstUnchosenSlot++;
				next = this.log.get(this.firstUnchosenSlot);
			}
		}
	}

	/**
	 * Play the log from the first unchosen slot until the next unchosen slot, or
	 * until the end.
	 */

	private void executeLog() { this.executeLog(true); }
	private void executeLog(boolean collect) {
		for (int slot = this.firstUnexecutedSlot; slot < this.firstEmptySlot; slot++) {
			PaxosInstance paxosEntry = this.log.get(slot);
			if (paxosEntry == null || !paxosEntry.chosen) {
				// Can't execute an unchosen (or absent) slot, and can't execute later ones.
				this.firstUnexecutedSlot = slot;
				break;
			} else {
				// Execute the command. It has been chosen.
				this.execute(paxosEntry);
				this.firstUnexecutedSlot = slot + 1;
			}
		}

		if (collect) {
			this.collectGarbage();
		}
	}

	private void collectGarbage() {
		int minExecuted = this.lastExecutedSlot;

		for (Integer peerExecutedSlot : this.peerExecutionInfo.values()) {
			minExecuted = Integer.min(minExecuted, peerExecutedSlot);
		}

		int minClientSlot = Integer.MAX_VALUE;

		for (Integer clientSlot : clientSlots.values()) {
			minClientSlot = Integer.min(minClientSlot, clientSlot);
		}

		LOG.fine(String.format("%s LOG collecting garbage slots %d -> %d. first unexecuted = %d. clientSlots=%s, peerExec=%s",
				this.address(), this.firstLogSlot, minExecuted, this.firstUnexecutedSlot, this.clientSlots, this.peerExecutionInfo));

		for (int slot = this.firstLogSlot; slot <= minExecuted; slot++) {

			if (slot >= minClientSlot) {
				break;
			}

			LOG.fine(String.format("%s LOG GC slotNum=%d", this.address(), slot));

			PaxosInstance instance = this.log.get(slot);
			assert instance != null;
			assert instance.executed;

			if (app != null) {
				this.app.execute(instance.proposal.proposalValue);
			}

			this.log.remove(slot);
			this.acceptedProposals.remove(slot);

			this.firstLogSlot = slot + 1;
		}
	}

	private void becomeLeader(){
		this.leaderConfirms = new HashSet<>();
		broadcastLeaderConfirmation(new LeaderConfirmation());
		set(new LeaderConfirmationTimeout());
	}

	public synchronized void updateLeaderStatus(){
		boolean inContactWithHigherServer = false;
		int     numberOfServersInContact  = 0;
		int     serversForMajority        = servers.length / 2;

		for(Map.Entry<Address, Integer> entry : heartbeats.entrySet()){
			//I'm still in contact:
			if(entry.getValue() <= 2){
				numberOfServersInContact++;
				//higher addressed server
				if(this.address().compareTo(entry.getKey()) < 0)
					inContactWithHigherServer = true;
			}
		}

		boolean inContactWithMajority = (numberOfServersInContact >= serversForMajority);

		//Im a leader or a candidate AND ive lost contact with majority or
		if ((serverRole == Role.LEADER || serverRole == Role.LEADER_CANDIDATE)
				&& (!inContactWithMajority || inContactWithHigherServer)) {
			LOG.fine("PEON DOWNGRADE " + address() + heartbeats.toString());
			serverRole = Role.PEON;

		} else if(serverRole == Role.PEON && inContactWithMajority && !inContactWithHigherServer){
			LOG.fine("TRY TO BE A LEADER FFS " + address() + " " + heartbeats);
			serverRole = Role.LEADER_CANDIDATE;
			becomeLeader();
		}
	}

	//-------------------------------------------------
	// Broadcast Helpers
	//-------------------------------------------------
	public void broadcastChosen(Chosen m){
		for(int i = 0; i < servers.length; i++){
			if(this.address().equals(servers[i])){
				handleChosen(m, this.address());
			}else{
				send(m, servers[i]);
			}
		}
	}

	public void broadcastAcceptRequest(AcceptRequest m){
		for(int i = 0; i < servers.length; i++){
			if(this.address().equals(servers[i])){
				handleAcceptRequest(m, this.address());
			}else{
				send(m, servers[i]);
			}
		}
	}

	public void broadcastLeaderConfirmation(LeaderConfirmation m) {
		for (int i = 0; i < servers.length; i++) {
			if (!leaderConfirms.contains(i)) {
				if (servers[i].equals(this.address())) {
					//handleLeaderConfirmation(m, this.address());
				} else {
					send(m, servers[i]);
				}
			}
		}
	}



	public void incrementHeartbeatMisses(){
		for(Map.Entry<Address, Integer> entry : heartbeats.entrySet()){
			heartbeats.put(entry.getKey(), entry.getValue()+1);
		}
	}

	public boolean currentLeader(){
		return serverRole == Role.LEADER;
	}
}
