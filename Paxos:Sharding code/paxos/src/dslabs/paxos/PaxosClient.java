package dslabs.paxos;

import dslabs.framework.Address;
import dslabs.framework.Client;
import dslabs.framework.Command;
import dslabs.framework.Node;
import dslabs.framework.Result;
import dslabs.atmostonce.AMOCommand;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.java.Log;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Log
public final class PaxosClient extends Node implements Client {
	private final Address[] servers;

	private PaxosRequest request;
	private PaxosReply   reply;
	private int          sequenceNum;
	private long startTime;

	/* -------------------------------------------------------------------------
		Construction and Initialization
	   -----------------------------------------------------------------------*/
	public PaxosClient(Address address, Address[] servers) {
		super(address);
		assert servers.length > 0;
		this.servers = servers;
		this.sequenceNum = 1;
	}

	@Override
	public synchronized void init() {
	}

	/* -------------------------------------------------------------------------
		Public methods
	   -----------------------------------------------------------------------*/
	@Override
	public synchronized void sendCommand(Command operation) {
		PaxosRequest r = new PaxosRequest(new AMOCommand(operation, sequenceNum++, this.address()));

		LOG.fine(String.format("sendCommand(%s)", r));

		startTime = System.currentTimeMillis();
		request = r;
		reply   = null;

		broadcast(r, servers);
		set(new ClientTimeout(r));
	}

	@Override
	public synchronized boolean hasResult() {
		return reply != null;
	}

	@Override
	public synchronized Result getResult() throws InterruptedException {
		while (!this.hasResult()){
			wait();
		}

		Result returnResult = reply.amoResult().result();
		reply = null;
		return returnResult;
	}

	/* -------------------------------------------------------------------------
		Message Handlers
	   -----------------------------------------------------------------------*/
	private synchronized void handlePaxosReply(PaxosReply m, Address sender) {
		LOG.fine(String.format("handlePaxosReply(%s, %s) { req=%s, reply=%s }",
				  m, sender, this.request, this.reply));

		if (request != null
				&& m.amoResult().sequenceNumber() == request.amoCommand().sequenceNumber()
				&& m.amoResult().clientAddress().equals(this.address())){

			long endTime = System.currentTimeMillis();
			LOG.fine(String.format("RESPONSE_TIME = %d ms", endTime - startTime));

			reply = m;
			request = null;
			notify();
		}
	}

	/* -------------------------------------------------------------------------
		Timeout Handlers
	   -----------------------------------------------------------------------*/
	private synchronized void onClientTimeout(ClientTimeout t) {
		LOG.fine(String.format("clienttimeout(%s) reply: %s request %s timeout request %s", this.address(), reply, request, t.request()));
		if(reply == null && request != null && t.request().equals(this.request)){
			broadcast(request, servers);
			set(t);
		}
	}
}
