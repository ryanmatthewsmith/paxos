package dslabs.paxos;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;

@ToString(callSuper = true)
@EqualsAndHashCode()
public class PaxosInstance implements Serializable{
   public Proposal     proposal;
   public boolean 	   chosen;
   public boolean      executed;
   public int          slotNum;

   public PaxosInstance(int slotNum){
      this.proposal      = new Proposal(-1, null);
      this.chosen        = false;
      this.executed      = false;
      this.slotNum       = slotNum;
   }
}
