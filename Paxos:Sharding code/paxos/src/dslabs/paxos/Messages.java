package dslabs.paxos;

import dslabs.framework.Address;
import dslabs.framework.Message;
import lombok.Data;

import java.util.HashMap;

@Data
class AcceptRequest implements Message{
    private final Proposal proposedValue;
    private final int      slotNum;
    private final int      firstUnchosenSlot;
}

@Data
class AcceptResponse implements Message{
    private final int      slotNum;
    private final boolean  accepted;
    private final Proposal acceptedProposal;
    private final boolean  chosen;
    private final int      firstUnchosenSlot;
}

@Data
class Chosen implements Message{
    private final int        slotNum;
    private final Proposal   chosenProposal;
}

@Data
class Heartbeat implements Message{
    private final int serverIndex;
    private final HashMap<Integer, PaxosInstance> log;
    private final HashMap<Address, Integer> clientSlots;
    private final int lastExecutedSlot;
    private final boolean isLeader;
    private final int firstUnchosenSlot;
    private final int firstEmptySlot;
}

@Data
class LeaderConfirmation implements Message {
}

@Data
class LeaderConfirmationResponse implements Message{
    private final int serverIndex;
    private final Heartbeat heartbeat;
}

@Data
final class PaxosDecision implements Message {

}
