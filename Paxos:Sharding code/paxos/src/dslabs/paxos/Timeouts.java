package dslabs.paxos;

import dslabs.framework.Timeout;

import lombok.Data;

@Data
final class ClientTimeout implements Timeout {
	private static final int CLIENT_RETRY_MILLIS = 100;

	private final PaxosRequest request;

	@Override
	public int timeoutLengthMillis() {
		return CLIENT_RETRY_MILLIS;
	}
}

@Data class HeartbeatTimeout implements Timeout {
	private static final int HEARTBEAT_LEADER_MILLIS = 40;

	@Override
	public int timeoutLengthMillis(){return HEARTBEAT_LEADER_MILLIS;}
}

@Data
class LeaderConfirmationTimeout implements Timeout {
	private static final int LEADER_CONFIRM_MILLIS = 130;

	@Override
	public int timeoutLengthMillis(){return LEADER_CONFIRM_MILLIS;}
}
